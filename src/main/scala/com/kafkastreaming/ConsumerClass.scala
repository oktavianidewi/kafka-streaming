package com.kafkastreaming
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.spark.sql.functions._

class ConsumerClass(brokers: String) {
    def process(): Unit = {
      val session = SparkSession
        .builder()
        .appName("kafka-tutorials")
        .master("local[*]")
        .getOrCreate()
      val kafkaDF = session.readStream
        .format("kafka")
        .option("kafka.bootstrap.servers", brokers)
        .option("subscribe", "persons")
        .load()
      kafkaDF.printSchema()
      val consoleOutput = kafkaDF.writeStream
        .format("console")
        .outputMode("append")
        .start()
      consoleOutput.awaitTermination()
      // kafkaDF
      //   .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
      //   .as[(String, String)]
      // display(kafkaDF.select($"key", $"value"))

    }
  }